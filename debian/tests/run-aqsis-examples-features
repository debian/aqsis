#!/bin/sh

# Author: Manuel A. Fernandez Montecelo <mafm@debian.org>
#
# autopkgtest check: Run (render) part of the examples


set -e

WORKDIR=$(mktemp -d)
trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM
cd $WORKDIR

SHADERS_DIR=/usr/share/aqsis/shaders
AQSIS_INVOCATION="aqsis -type file"

cp -a /usr/share/aqsis/examples/features .

for dir_test in features/*; do
    (
	echo "entering: ${dir_test}"
	cd "${dir_test}"

	for render in render*.sh; do
	    render_path="${dir_test}/${render}"
	    case "${render_path}" in
		*/bake/render.sh|*/occlusion/render.sh|*render_deformation.sh|*render_softshadow.sh)
		    echo "Skip script known to fail: ${render_path}"
		    continue
		    ;;
	    esac

	    sed -i "/^aqsis/ s|^aqsis|${AQSIS_INVOCATION} -shaders=${SHADERS_DIR}/displacement:${SHADERS_DIR}/light:${SHADERS_DIR}/surface|" "${render}"
	    sed -i "/^aqsis/ s|-progress||" "${render}"
	    sed -i "/^aqsl/ s|\.\./\.\./\.\./|/usr/share/aqsis/|" "${render}"

	    echo "$(date): running: ${render_path}"
	    ./"${render}"
	done
    )
done


echo "run: OK"
